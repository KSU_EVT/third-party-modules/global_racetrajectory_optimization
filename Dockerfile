# Dockerfile

# Use an official Python runtime as a parent image
FROM my_intermediate_image

# Set the working directory in the container to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Run main_globaltraj.py when the container launches
CMD ["python", "./main_globaltraj.py"]

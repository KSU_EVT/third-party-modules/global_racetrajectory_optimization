# Required Imports
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import PolygonStamped
import csv

class Polygon2CSVNode(Node):
    def __init__(self):
        super().__init__('polygon2csv_node')
        self.subscription = self.create_subscription(PolygonStamped, '/slime_trail', self.listener_callback, 10)

    def listener_callback(self, msg):
        with open('perdue-raw.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["x_m", "y_m", "w_tr_right_m", "w_tr_left_m"])  # Write the Header
            for point in msg.polygon.points:
                x = point.x
                y = point.y
                writer.writerow([x, y, 3.0, 3.0])  # Write coordinates and fixed values

def main(args=None):
    rclpy.init(args=args)
    polygon2csv_node = Polygon2CSVNode()
    rclpy.spin(polygon2csv_node)
    polygon2csv_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()

#!/usr/bin/env bash

# Build Docker image named "my_python_app"
docker build -t global_racetrajectory_optimization .

# Run your arbitrary Python script to convert the file
# add header:
# x_m,y_m,w_tr_right_m,w_tr_left_m
# add columns 3 and 4 with 3.0
ros2 run --prefix "python3" ./polygon_to_csv.py

# Run the Docker container with a volume mount
docker run -v "$(pwd)/perdue-raw.csv:/app/inputs/tracks/perdue-raw.csv" global_racetrajectory_optimization
